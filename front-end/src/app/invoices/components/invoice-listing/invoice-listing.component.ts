import { Component, OnInit } from '@angular/core';
import { InvoiceService } from '../../services/invoice.service';
import { Invoice } from '../../models/invoice';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { take } from 'rxjs/operators';


@Component({
  selector: 'app-invoice-listing',
  templateUrl: './invoice-listing.component.html',
  styleUrls: ['./invoice-listing.component.scss']
})
export class InvoiceListingComponent implements OnInit {

  displayedColumns: string[] = ['item', 'qty', 'date', 'due', 'tax', 'rate', 'action'];
  dataSource: Array<Invoice>;

  constructor(private invoiceService: InvoiceService,
    private router: Router,
    private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.invoiceService.getInvoices()
      .subscribe(invoices => {
        this.dataSource = invoices;
      });
  }

  onDelete(id: string): void {
    this.invoiceService
      .deleteInvoice(id)
      .pipe(take(1))
      .subscribe(data => {

        const emloyeeIdx = this.dataSource
          .findIndex(invoice => invoice._id === id);
        this.dataSource.splice(emloyeeIdx, 1);
        this.dataSource = [...this.dataSource];

        this.snackbar.open('Invoice delete', 'Successs', {
          duration: 2000
        });
      }, err => {
      });
  }

  onSave(): void {
    this.router.navigate(['dashboard', 'invoices', 'new']);
  }

}
