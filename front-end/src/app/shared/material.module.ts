import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// tslint:disable-next-line: max-line-length
import { MatButtonModule, MatTableModule, MatSidenavModule, MatToolbarModule, MatListModule, MatIconModule, MatCardModule, MatMenuModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatSnackBarModule } from '@angular/material';

const MAT_MODULES = [
  [
    CommonModule,
    MatButtonModule,
    MatTableModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatSnackBarModule
  ]
];

@NgModule({
  declarations: [],
  imports: MAT_MODULES,
  exports: [
    MAT_MODULES
  ]
})
export class MaterialModule { }
