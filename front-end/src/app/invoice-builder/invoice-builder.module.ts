import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceBuilderComponent } from './invoice-builder.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { InvoiceRoutingModule } from './invoice-builder-routing.module';
import { MaterialModule } from '../shared/material.module';
import { InvoicesModule } from '../invoices/invoices.module';
import { ClientsModule } from '../clients/clients.module';



@NgModule({
  declarations: [InvoiceBuilderComponent, MainContentComponent, SideNavComponent, ToolbarComponent],
  imports: [
    CommonModule,
    InvoiceRoutingModule,
    InvoicesModule,
    ClientsModule,
    MaterialModule
  ]
})
export class InvoiceBuilderModule { }
