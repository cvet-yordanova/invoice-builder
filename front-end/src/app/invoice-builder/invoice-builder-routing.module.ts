import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainContentComponent } from './components/main-content/main-content.component';
import { InvoiceBuilderComponent } from './invoice-builder.component';
import { InvoiceListingComponent } from '../invoices/components/invoice-listing/invoice-listing.component';
import { ClientListingComponent } from '../clients/client-listing/client-listing.component';
import { InvoiceFormComponent } from '../invoices/components/invoice-form/invoice-form.component';


const routes: Routes = [{
  path: '',
  component: InvoiceBuilderComponent,
  children: [
    {
      path: '',
      component: MainContentComponent
    },
    {
      path: 'invoices',
      component: InvoiceListingComponent
    },
    {
      path: 'invoices/new',
      component: InvoiceFormComponent
    },
    {
      path: 'clients',
      component: ClientListingComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoiceRoutingModule { }
