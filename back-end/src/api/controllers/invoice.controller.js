const Invoice = require("../models/invoice.model");

module.exports = {
  findAll: function findAll(req, res, next) {
    Invoice.find()
      .then(invoices => res.json(invoices))
      .catch(err => res.status(500).json(err));
  },
  findOne: function(req, res) {
    const { id } = req.params;
    Invoice.findById(id)
      .then(invoice => {
        if (!invoice) {
          return res.status(404).json({ err: "Could not find any invoice" });
        }
        return res.json(invoice);
      })
      .catch(err => res.status(500).json(err));
  },
  create: function(req, res, next) {
    const { item, qty, date, due, tax, rate } = req.body;

    if (!item) {
      return res.status(400).json({ err: "Item is required" });
    }

    if (!date) {
      return res.status(400).json({ err: "Date is required" });
    }

    if (!due) {
      return res.status(400).json({ err: "Due date is required" });
    }

    if (!qty) {
      return res.status(400).json({ err: "Qty is required" });
    }

    Invoice.create({ item, qty, date, due, tax, rate })
      .then(invoice => {
        res.json(invoice);
      })
      .catch(err => {
        err.status(500).json(err);
      });
  },
  delete: function(req, res) {
    const { id } = req.params;
    Invoice.findByIdAndRemove(id)
      .then(invoice => {
        if (!invoice) {
          return res.status(404).json({ err: "Could not delete any invoice" });
        }

        return res.json(invoice);
      })
      .catch(err => res.status(500).json(err));
  },
  update: function(req, res) {
    //todo validation
    // const { id } = req.params;
    // Invoice.findByIdAndUpdate({ _id: id }, value, { new: true })
    //   .then(invoice => res.json(invoice))
    //   .catch(err => res.status(500).json(err));
  }
};
