const express = require("express");
const router = require("./config/routes");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const port = 4000;

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/invoice-builder", {
  useNewUrlParser: true
});

app.listen(port, () => {
  console.log(`Server is running at port ${port}`);
});

app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api", router);
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);

  return res.json({
    error: {
      message: error.message
    }
  });
});

app.get("/invoices", (req, res) => {
  res.json(invoices);
});

app.get("/", (req, res) => {
  res.json({
    msg: "Welcome"
  });
});
